===============================================
| RC AREA 2 
===============================================
		(Idea from Amco Bitume)

   Author: Ayumu
   Length: 250m
   Diff. : Hard
   

	Elapsed time:  1 week of work (and eventually 2 other weeks of AI)
        Tools used  : 3Ds max 2011, Paint.NET, Notepad, Re-Volt's MAKEITGOOD (and W_Console).
	

   Intro
_________

 RC Area challenge for Master Rc racers is open
 Not everybody can race because track is very difficult .

 PS: There is another rc area! this time is very easy!


  Advice
_________

 Use cars with good springs and suspensions 


  Material
__________

     audio: Evening in the forest MP3 by reinsamba (please download 256x256 version)
            because file size limit is 7MB in RVZT
      textures:
	- tree textures: from `Tree pack' by killst4r
	- Acclaim, Re-Volt logo and cars: from revolt 1.2
	- people: from got3d.com
	- Roof texture: from ?



  Thank
________

 s3.invisionfree.com/Revolt_Live for their kind words
 Acclaim for the great game
 revolt.webuda.com for tutorials


 KDL for help in texanim
 miromiro for help in AI nodes (computer track soon)

 
  Contact
________

 ayumeru16@gmail.com
