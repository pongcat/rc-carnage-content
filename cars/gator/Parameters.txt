{

;============================================================
;============================================================
; Gator-Raid
;============================================================
;============================================================
Name       	"Gator-Raid"


;====================
; Stuff mainly for frontend display and car selectability
;====================

BestTime   	true
Selectable 	true
;)CPUSelectable	true
;)Statistics 	true
Class      	1 			; Engine type (0=Elec, 1=Glow, 2=Other)
Obtain     	0 			; Obtain method
Rating     	3 			; Skill level (rookie, amateur, ...)
TopEnd     	3324.375977 		; Actual top speed (mph) for frontend bars
Acc        	5.568226 		; Acceleration rating (empirical)
Weight     	1.400000 		; Scaled weight (for frontend bars)
Trans      	0 			; Transmission type (0=4WD, 1=FWD, 2=RWD)
MaxRevs    	0.500000 		; Max Revs (for rev counter, deprecated...)

;====================
; Model Filenames
;====================

MODEL 	0 	"cars/Gator/body.prm"
MODEL 	1 	"cars/Gator/wheelfl.prm"
MODEL 	2 	"cars/Gator/wheelfr.prm"
MODEL 	3 	"cars/Gator/wheelbl.prm"
MODEL 	4 	"cars/Gator/wheelbr.prm"
MODEL 	5 	"cars/Gator/spring.prm"
MODEL 	6 	"NONE"
MODEL 	7 	"NONE"
MODEL 	8 	"NONE"
MODEL 	9 	"cars/Gator/axle.prm"
MODEL 	10 	"cars/Gator/axle_B.prm"
MODEL 	11 	"NONE"
MODEL 	12 	"NONE"
MODEL 	13 	"NONE"
MODEL 	14 	"NONE"
MODEL 	15 	"NONE"
MODEL 	16 	"NONE"
MODEL 	17 	"cars/misc/Aerial.m"
MODEL 	18 	"cars/misc/Aerialt.m"
COLL 		"cars/Gator/hull.hul"
TPAGE 		"cars/Gator/car.bmp"
;)TCARBOX  	"cars/Gator/carbox.bmp" 			; Carbox texture
;)TSHADOW 	"cars/Gator/shadow.bmp"
;)SHADOWINDEX 	-1 			; Use a default shadow (0 to 27 or -1)
;)SHADOWTABLE 	-46.700001 46.700001 54.000000 -49.000000 0.000000 	; Left, right, front, back, height (relative to model center)
;)SFXENGINE 	"NONE"
EnvRGB 		250 250 250

;====================
; Handling related stuff
;====================

SteerRate  	2.400000 			; Rate at which steer angle approaches value from input
SteerMod   	0.400000 			;
EngineRate 	4.500000 			; Rate at which Engine voltage approaches set value
TopSpeed   	38.500000 			; Car's theoretical top speed (not including friction...)
DownForceMod	2.000000 			; Down force modifier when car on floor
CoM        	0.000000 -4.000000 -4.000000 	; Centre of mass relative to model centre
Weapon     	0.000000 -32.000000 64.000000 	; Weapon generation offset
;)Flippable	false 		; Rotor car effect
;)Flying   	false 		; Flying like the UFO car
;)ClothFx  	false 		; Mystery car cloth effect

;====================
; Camera details
;====================

;)CAMATTACHED {	; Start Camera
;)HoodOffset   	0.000000 0.000000 0.000000 	; Offset from model center
;)HoodLook     	0.050000 			; Look angle (-0.25 to 0.25, 0.0 - straight ahead)
;)RearOffset   	0.000000 0.000000 0.000000
;)RearLook     	0.050000
;)UseDefault   	false 				; Use default offsets (computed in game)
;)}            	; End Camera

;====================
; Car Body details
;====================

BODY {		; Start Body
ModelNum   	0 			; Model Number in above list
Offset     	0.000000 0.000000 0.000000
Mass       	1.800000
Inertia    	850.000000 0.000000 0.000000
           	0.000000 1520.000000 0.000000
           	0.000000 0.000000 1280.000000
Gravity    	2200 			; No longer used
Hardness   	-0.400000
Resistance 	0.000800 		; Linear air resistance
AngRes     	0.000900 		; Angular air resistance
ResMod     	25.000000 		; AngRes scale when in air
Grip       	0.015000 		; Converts downforce to friction value
StaticFriction 	0.800000
KineticFriction 0.400000
}     		; End Body

;====================
; Car Wheel details
;====================

WHEEL 0 { 	; Start Wheel
ModelNum 	1
Offset1  	-22.000000 2.000000 30.000000
Offset2  	-5.000000 0.000000 0.000000
IsPresent   	true
IsPowered   	true
IsTurnable  	true
SteerRatio  	-0.350000
EngineRatio 	20000.000000
Radius      	14.000000
Mass        	0.150000
Gravity     	2000.000000
MaxPos      	12.000000
SkidWidth   	11.000000
ToeIn       	0.000000
;)Camber    	-2.000000
AxleFriction    0.010000
Grip            0.012000
StaticFriction  1.820000
KineticFriction 1.680000
}          	; End Wheel

WHEEL 1 { 	; Start Wheel
ModelNum 	2
Offset1  	22.000000 2.000000 30.000000
Offset2  	5.000000 0.000000 0.000000
IsPresent   	true
IsPowered   	true
IsTurnable  	true
SteerRatio  	-0.350000
EngineRatio 	20000.000000
Radius      	14.000000
Mass        	0.150000
Gravity     	2000.000000
MaxPos      	12.000000
SkidWidth   	11.000000
ToeIn       	0.000000
;)Camber    	-2.000000
AxleFriction    0.010000
Grip            0.012000
StaticFriction  1.820000
KineticFriction 1.680000
}          	; End Wheel

WHEEL 2 { 	; Start Wheel
ModelNum 	3
Offset1  	-24.000000 4.000000 -28.000000
Offset2  	-5.000000 0.000000 0.000000
IsPresent   	true
IsPowered   	true
IsTurnable  	false
SteerRatio  	0.100000
EngineRatio 	22000.000000
Radius      	14.000000
Mass        	0.150000
Gravity     	2000.000000
MaxPos      	12.000000
SkidWidth   	11.000000
ToeIn       	0.000000
;)Camber    	-2.000000
AxleFriction    0.025000
Grip            0.013500
StaticFriction  1.740000
KineticFriction 1.620000
}          	; End Wheel

WHEEL 3 { 	; Start Wheel
ModelNum 	4
Offset1  	24.000000 4.000000 -28.000000
Offset2  	5.000000 0.000000 0.000000
IsPresent   	true
IsPowered   	true
IsTurnable  	false
SteerRatio  	0.100000
EngineRatio 	22000.000000
Radius      	14.000000
Mass        	0.150000
Gravity     	2000.000000
MaxPos      	12.000000
SkidWidth   	11.000000
ToeIn       	0.000000
;)Camber    	-2.000000
AxleFriction    0.025000
Grip            0.013500
StaticFriction  1.740000
KineticFriction 1.620000
}          	; End Wheel


;====================
; Car Spring details
;====================

SPRING 0 { 	; Start Spring
ModelNum    	5
Offset      	-10.000000 -12.000000 27.799999
Length      	20.000000
Stiffness   	150.000000
Damping     	10.000000
Restitution 	-0.750000
}           	; End Spring

SPRING 1 { 	; Start Spring
ModelNum    	5
Offset      	10.000000 -12.000000 27.799999
Length      	20.000000
Stiffness   	150.000000
Damping     	10.000000
Restitution 	-0.750000
}           	; End Spring

SPRING 2 { 	; Start Spring
ModelNum    	5
Offset      	-10.000000 -12.000000 -26.000000
Length      	20.000000
Stiffness   	200.000000
Damping     	5.000000
Restitution 	-0.750000
}           	; End Spring

SPRING 3 { 	; Start Spring
ModelNum    	5
Offset      	10.000000 -12.000000 -26.000000
Length      	20.000000
Stiffness   	200.000000
Damping     	5.000000
Restitution 	-0.750000
}           	; End Spring


;====================
; Car Pin details
;====================

PIN 0 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 1 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 2 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 3 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin


;====================
; Car axle details
;====================

AXLE 0 {   	; Start Axle
ModelNum    	10
Offset      	-2.000000 -3.000000 30.000000
Length      	20.000000
}           	; End axle

AXLE 1 {   	; Start Axle
ModelNum    	9
Offset      	2.000000 -3.000000 30.000000
Length      	20.000000
}           	; End axle

AXLE 2 {   	; Start Axle
ModelNum    	9
Offset      	-2.000000 -3.000000 -28.000000
Length      	20.000000
}           	; End axle

AXLE 3 {   	; Start Axle
ModelNum    	10
Offset      	2.000000 -3.000000 -28.000000
Length      	20.000000
}           	; End axle


;====================
; Car spinner details
;====================

SPINNER {   	; Start spinner
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Axis        	0.000000 0.000000 0.000000
AngVel      	0.000000
;)Type      	4 				; 1: Default rot, 2: Turn with steer, 4: Translate with speed, 6: 2 and 4
;)Trans     	0.000000 0.000000 0.000000 	; Translation max
;)TransVel  	0.000000 			; Velocity factor
}           	; End Spinner


;====================
; Car Aerial details
;====================

AERIAL {    	; Start Aerial
SecModelNum 	17
TopModelNum 	18
Offset      	10.000000 -20.000000 -25.000000
Direction   	0.000000 -1.000000 0.000000
Length      	20.000000
Stiffness   	2000.000000
Damping     	5.500000
}           	; End Aerial

;====================
;Camera
;====================

;)CAMATTACHED { ; Start Camera
;)HoodOffset 0.00 -55.00 -222.00
;)HoodLook 0.00
;)RearOffset 0.00 150.00 300.00
;)RearLook 0.04
;)} ; End Camera


;====================
; Car AI details
;====================

AI {        	; Start AI
UnderThresh 	5.000000
UnderRange  	1584.415039
UnderFront  	401.940002
UnderRear   	321.519989
UnderMax    	0.283685
OverThresh  	1954.575073
OverRange   	1541.581543
OverMax     	1.000000
OverAccThresh  	131.029999
OverAccRange   	1928.010010
PickupBias     	16383
BlockBias      	16383
OvertakeBias   	16383
Suspension     	26213
Aggression     	0
}           	; End AI

}

